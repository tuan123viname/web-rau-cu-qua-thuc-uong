const categoryModel=require('../../model/category.model');
const productModel=require('../../model/product.model');

function createCategory(body)
{
    return new Promise((resolve,reject)=>{
        categoryModel.findOne({name:body.name})
        .then(founded=>{
            if(founded)
            {
                return reject({message:'danh muc da ton tai'});
            }
            else
            {
                const category=new categoryModel({
                    name:body.name,
                    type:body.type
                });
                category.save()
                .then(data=>{
                    return resolve(
                        {
                            message:'tao danh muc thanh cong',
                            data
                         })
                })
            }
        })
        .catch(err=>{
            return reject(err);
        })
    })
}

function deleteCategory(id)
{
    return new Promise((resolve,reject)=>{
        categoryModel.findByIdAndDelete(id)
        .then(data=>{
            return resolve({message:'xoa danh muc thanh cong'});
        })
        .catch(err=>{
            return reject({message:'id danh muc khong hop le'})
        })
    })
}

function addProductIntoCategory(body)
{
    return new Promise((resolve,reject)=>{
        productModel.findById(body.id)
        .then(product=>{
            if(!product)
            {
                return reject({message:'ban phai tao san pham nay truoc thi them no vao danh muc'});
            }
            else
            {
                categoryModel.findOne({type:body.type})
                .then(founded=>{
                    if(!founded)
                    {
                        return reject({message:'danh muc khong ton tai'});
                    }
                    else
                    {
                        for(let item of founded.product)
                        {
                            if(item==body.id)
                            {
                                return reject({message:'san pham da duoc them vao danh muc truoc do'});
                            }
                        }
                        founded.product.push(body.id);
                        founded.save()
                        .then(data=>{
                            return resolve(
                                {
                                    message:'them san pham vao danh muc thanh cong',
                                    data:data
                                }
                             );
                        })
                        .catch(err=>{
                          
                            return reject({message:'them san pham vao danh muc that bai'});
                        })
                    }
                })
                .catch(err=>{
                    return reject({message:'khong the tim kiem '})
                })
            }
        })
        .catch(err=>{
            return reject({message:'id san pham khong hop le'});
        })
        
    })
    
}

function removeProduct(body)
{
    return new Promise((resolve,reject)=>{
        productModel.findById(body.id)
        .then(product=>{
            if(!product)
            {
                return reject({message:'san pham ban xoa khong ton tai'});
            }
            else
            {
                categoryModel.findOne({type:body.type})
                .then(founded=>{
                    if(!founded)
                    {
                        return reject({message:'danh muc khong ton tai'});
                    }
                    else
                    {
                        for(let i=0;i<founded.product.length;i++)
                        {
                            if(founded.product[i]==body.id)
                            {
                                founded.product.splice(i,1);
                                return founded.save()
                                .then(data=>{
                                    return resolve({message:'xoa thanh cong',data});
                                })
                                .catch(err=>{
                                    return reject({message:'xoa khong thanh cong'});
                                })
                            }
                        }
                        
                        return reject({message:'id san pham khong ton tai'});
                    }
                })
                .catch(err=>{
                    return reject({message:'id san pham khong hop le'});
                })
            }
        })
        .catch(err=>{
            return reject(err);
        })
      
    })
}

module.exports={
    createCategory         : createCategory,
    deleteCategory         : deleteCategory,
    addProductIntoCategory : addProductIntoCategory,
    removeProduct          : removeProduct
}