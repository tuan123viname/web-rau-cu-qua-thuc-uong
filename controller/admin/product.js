const productModel=require('../../model/product.model');
const categoryModel=require('../../model/category.model');

function createProduct(body){
    return new Promise((resolve,reject)=>{
        productModel.findOne({name:body.name})
        .then(founded=>{
            if(founded)
            {
                return reject({message:'ten san pham da ton tai'});
            }
            else{
                const product=new productModel({
                    name:body.name,
                    price:body.price,
                    discount:body.discount,
                    description:body.description,
                    image:body.image,
                    date:body.date
                })
                product.save()
                .then(data=>{
                    return resolve({
                        message:'tao san pham thanh cong',
                        data
                    });
                })
                .catch(err=>{
                    return reject(err);
                })
            }
        })
        .catch(err=>{
            return reject(err);
        })
    })
}

function deleteProduct(id){
    return new Promise((resolve,reject)=>{
        productModel.findByIdAndDelete(id)
        .then(data=>{
            return resolve({message:'xoa san pham thanh cong'});
        })
        .catch(err=>{
            return reject({message:'id san pham khong hop le'});
        })
    })
}

function updateProduct(body){
    return new Promise((resolve,reject)=>{
        productModel.findByIdAndUpdate(body.id,body,{new:true})
        .then(data=>{
            return resolve({message:'cap nhat san pham thanh cong',data});
        })
        .catch(err=>{
            return reject({message:'id san pham khong ton tai'});
        })
    })
}   

module.exports={
    createProduct :  createProduct,
    updateProduct : updateProduct ,
    deleteProduct : deleteProduct
}