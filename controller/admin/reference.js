const referenceModel=require('../../model/reference.model');
function createReference(body){
    return new Promise((resolve,reject)=>{
        referenceModel.findOne({title:body.title})
        .then(founded=>{
            if(founded){
                console.log('tieu de tham khao da ton tai');
            }
            else{
                const reference=new referenceModel({
                    title:body.title,
                    image:body.image,
                    content:body.content,
                    date:body.date
                })
                reference.save()
                .then(data=>{
                    return resolve({message:'tao thanh cong reference',data});
                })
                .catch(err=>{
                    return reject(err);
                })
            }
        })
        .catch(err=>{
            return reject(err);
        })
    })
}

function updateReference(body){
    return new Promise((resolve,reject)=>{
        referenceModel.findByIdAndUpdate(body.id)
        .then(founded=>{
            if(founded){
                founded.date=body.date;
                founded.title=body.title;
                founded.image=body.image;
                founded.content=body.content;
                founded.save()
                .then(data=>{
                    return resolve({message:"cap nhat thanh cong",data});
                })
                .catch(err=>{
                    return reject(err);
                })
            }
            else{
                return reject({message:"khong tim thay id cua tham khao can update"});
            }
        })
        .catch(err=>{
            return reject(err);
        })
    })
}
function deleteReference(body){
    return new Promise((resolve,reject)=>{
        referenceModel.findByIdAndDelete(body.id)
        .then(()=>{
            return resolve({message:'ban da xoa thanh cong'});
        })
        .catch(err=>{
            return reject({message:"id tham khao khong hop le"},err);
        })
    })
}


module.exports={
    createReference:createReference,
    updateReference:updateReference
}