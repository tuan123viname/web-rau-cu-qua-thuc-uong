const userModel=require('../../model/user.model');
const bcryptjs=require('bcryptjs');

function getUser(id){
    return new Promise((resolve,reject)=>{
        userModel.findById(id)
        .then(founded=>{
            if(!founded){
                return reject({message:'Tài khoản không tồn tại.'});
            }
            else{
                return resolve(founded);
            }
        })
        .catch(err=>{
            return reject(err);
        })

    })
}

function createUser(body){
    return new Promise((resolve,reject)=>{
        userModel.findOne({email:body.email})
        .then(founded=>{
            if(founded)
            {
                return reject({message:'Email đã tồn tại!'});
            }
            else{
                bcryptjs.hash(body.password,10)
                .then(hash=>{
                    const User=new userModel({
                        email:body.email,
                        password:hash
                    });
                    User.save()
                    .then(data=>{
                        return resolve({message:'Tạo tài khoản thành công!',data});
                    })
                    .catch(err=>{
                        return reject(err);
                    })
                })
                .catch(err=>{
                    return reject(err);
                })
              
            }
        })
        .catch(err=>{
            return reject(err);
        })
    })
}

function updateUser(body){
    return new Promise((resolve,reject)=>{
        userModel.findByIdAndUpdate(body.id)
        .then(founded=>{
            if(!founded){
                return  reject({message:'ID tài khoản không tồn tại.'})
            }
            else{
                if(body.email){founded.email=body.email};
                if(body.password){founded.password=body.password};
                if(body.name){founded.name=body.name};
                if(body.avatar){founded.avatar=body.avatar}
                founded.save()
                .then(data=>{
                    return resolve({message:'Cập nhật thành công.',data});
                })
                .catch(err=>{
                    return reject(err);
                })
            }
        })
        .catch(err=>{
            return reject(err);
        })
    })
}

module.exports={
    createUser:createUser,
    updateUser:updateUser,
    getUser:getUser
}