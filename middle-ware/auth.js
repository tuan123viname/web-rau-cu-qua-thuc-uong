const jwt=require('../untils/jwt');
const userController=require('../controller/admin/user');

function authUser(req,res,next)
{
    const token=req.headers['x-access-token'];
    jwt.verify(token,(err,decode)=>{
        if(err)
        {
            res.json({message:'ban phai dang nhap de su dung chuc nang nay'});
        }
        else
        {
            userController.findUserByEmail(decode.email)
            .then(user=>{
                if(!user.email)
                {
                    res.json({message:'email khong chinh xac'});
                }
                else
                {
                    next();
                }
            })
        }
    })
}

function authAdmin(req,res,next)
{
    const token=req.headers['x-access-token'];
    jwt.verify(token,(err,decode)=>{
        if(err)
        {
            res.json({message:'ban phai dang nhap de su dung chuc nang nay'});
        }
        else
        {
            userController.findUserByEmail(decode.email)
            .then(user=>{
                if(!user.email)
                {
                    res.json({message:'email khong chinh xac'});
                }
                else
                {
                   if(user.role===1)
                   {
                      next();   
                   }
                   else
                   {
                       res.json({message:'ban phai la admin de su dung chuc nang nay'});
                   }
                }
            })
        }
    })
}



module.exports={
    authUser  : authUser,
    authAdmin : authAdmin
}