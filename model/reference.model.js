const mongoose=require('mongoose');
const referenceSchema=new mongoose.Schema({
    title:{
        type:String,
        required:true
    },
    image:[],
    content:{
        type:String,
        required:true
    },
    date:{
        type:String,
        default:''
    }

})
module.exports=mongoose.model('Reference',referenceSchema);