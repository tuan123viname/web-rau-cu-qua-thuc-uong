const moongoose=require('mongoose');
const userSchema=new moongoose.Schema({
    email:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    name:String,
    address:String,
    phoneNumber:Number,
    role:{
        type:Number,
        default:0
    },
    active:{
        type:Boolean,
        default:true
    },
    avatar:{
        type:String,
        default:""
    }
});
module.exports=moongoose.model("User",userSchema);