const Router=require('express').Router();
const categoryController=require('../../controller/admin/category');

Router.post('/them-danh-muc',createCategory);
Router.delete('/xoa-danh-muc',deleteCategory);
Router.put('/them-san-pham',addProduct);
Router.delete('/xoa-san-pham',removeProduct);

function createCategory(req,res,next)
{
    const category=req.body;
    categoryController.createCategory(category)
    .then(data=>{
        res.json(data);
    })
    .catch(err=>{
        res.json(err);
    })
}

function addProduct(req,res,next)
{
    categoryController.addProductIntoCategory(req.body)
    .then(data=>{
        res.json(data);
    })
    .catch(err=>{
        res.json(err);
    })
}

function removeProduct(req,res,next)
{
    categoryController.removeProduct(req.body)
    .then(data=>{
        res.json(data);
    })
    .catch(err=>{
        res.json(err);
    })
}

function deleteCategory(req,res,next)
{
    categoryController.deleteCategory(req.body.id)
    .then(data=>{
        res.json(data);
    })
    .catch(err=>{
        res.json(err);
    })
}

module.exports=Router;