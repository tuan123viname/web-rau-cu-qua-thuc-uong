const Router=require('express').Router();

const productController=require('../../controller/admin/product');

Router.post('/',createProduct);
Router.put('/',updateProduct);
Router.delete('/',deleteProduct);
function createProduct(req,res,next)
{
    productController.createProduct(req.body)
    .then(data=>{
        res.json(data);
    })
    .catch(err=>{
        res.json(err);
    })

}

function updateProduct(req,res,next){
    productController.updateProduct(req.body)
    .then(data=>{
        res.json(data);
    })
    .catch(err=>{
        res.json(err);
    })
}

function deleteProduct(req,res,next)
{
    productController.deleteProduct(req.body.id)
    .then(data=>{
        res.json(data);
    })
    .catch(err=>{
        res.json(err);
    })
}
module.exports=Router;