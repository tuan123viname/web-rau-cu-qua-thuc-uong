const Router=require('express').Router();
const referenceController=require('../../controller/admin/reference')

Router.post('/',createReference);
Router.put('/',updateReference);
Router.delete('/',deleteReference);

function createReference(req,res,next){
    const reference=req.body;
    referenceController.createReference(reference)
    .then(data=>{
        res.json(data);
    })
    .catch(err=>{
        res.json(err);
    })
}
 
function updateReference(req,res,next){
    const reference=req.body;
    referenceController.updateReference(reference)
    .then(data=>{
        res.json(data);
    })
    .catch(err=>{
        res.json(err);
    })
}
function deleteReference(req,res,next){
    const reference=req.body;
    referenceController.deleteReference(reference)
    .then(data=>{
        res.json(data);
    })
    .catch(err=>{
        res.json(err);
    })
}



module.exports=Router;