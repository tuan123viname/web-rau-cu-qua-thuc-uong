const Router=require('express').Router();

const adminCategoryRouter=require('./admin/category');
const userCategoryRouter=require('./user/category');
const userReferenceRouter=require('./user/reference');
const adminReferenceRouter=require('./admin/reference');
const userRouter=require('./user/user');
const userAdminRouter=require('./admin/user');
const loginRouter=require('./user/login');
const productAdminRouter=require('./admin/product');
const referenceDetailRouter=require('./user/reference');


Router.use('/admin/san-pham',productAdminRouter);
Router.use('/admin/danh-muc',adminCategoryRouter);
Router.use('/admin/tham-khao',adminReferenceRouter);
Router.use('/admin/user',userAdminRouter);

Router.use('/tham-khao',userReferenceRouter);
Router.use('/tao-tai-khoan',userRouter);
Router.use('/thay-doi-thong-tin',userRouter);
Router.use('/tham-khao',referenceDetailRouter);
Router.use('/thong-tin-ca-nhan',userRouter);
//Router.use('/admin/tham-khao',adminReferenceRouter);
//Router.use('/admin/quan-ly-user',userAdminRouter);

Router.use('/danh-muc',userCategoryRouter);
//Router.use('/tham-khao',userReferenceRouter);
//Router.use('/thong-tin-ca-nhan',userRouter);
Router.use('/login',loginRouter);

module.exports=Router;