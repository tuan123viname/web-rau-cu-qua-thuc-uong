const Router=require('express').Router();
const userController=require('../../controller/user/user');

Router.put('/',updateUser);
Router.post('/',createUser);
Router.get('/:id',getUser);


function getUser(req,res,next){
    const user=req.params.id;
    userController.getUser(user)
    .then(data=>{
        res.json(data);
    })
    .catch(err=>{
        res.json(err);
    })
}

function createUser(req,res,next){
    const user=req.body;
    userController.createUser(user)
    .then(data=>{
        res.json(data);
    })
    .catch(err=>{
        res.json(err);
    })
}


function updateUser(req,res,next){
    const user=req.body;
    userController.updateUser(user)
    .then(data=>{
        res.json(data);
    })
    .catch(err=>{
        res.json(err);
    })
}




module.exports=Router;